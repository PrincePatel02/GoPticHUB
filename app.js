// Requiring the module
const express = require("express");
const reader = require("xlsx");
const fs = require("fs");
const parser = require("body-parser");
const path = require("path");
const multer = require("multer");
const cors = require("cors");
const app = express();

app.use(parser.urlencoded({ extended: false }));
app.use(parser.json());
app.use(cors());

// View Engine Setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use("/results", express.static("results"));

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    // Uploads is the Upload_folder_name
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname +
        "-" +
        Date.now() +
        path.extname(file.originalname).toLowerCase()
    );
  },
});

const maxSize = 5 * 1000 * 1000;

const upload = multer({
  storage: storage,
  limits: { fileSize: maxSize },
  fileFilter: (req, file, cb) => {
    if (path.extname(file.originalname).toLowerCase() !== ".xlsx") {
      return cb(new Error("Only xlsx are allowed"));
    }

    cb(null, true);

    // Set the filetypes, it is optional
    // const filetypes = /xlsx/;
    // const mimetype = filetypes.test(file.mimetype);
    // console.log("mimetype", file.mimetype);
    // const extname = filetypes.test(
    //   path.extname(file.originalname).toLowerCase()
    // );
    // if (mimetype && extname) {
    //   return cb(null, true);
    // }

    // cb(
    //   new Error("Error: File upload only supports the " +
    //     "following filetypes - " +
    //     filetypes)
    // );
  },
}).single("myfile");

const range = (start, end, step) => {
  const arrayLength = Math.floor((end - start) / step) + 1;
  return [...Array(arrayLength).keys()].map((x) => x * step + start);
};

const microData = (rows, column) => {
  const data = [];
  for (const row of rows) {
    if (row[column] && row[column].toString().indexOf(",") != -1) {
      for (const d of row[column].split(",")) {
        data.push({ ...row, [column]: d.trim() });
      }
    } else if (row[column] && /\d+-\d+/.test(row[column])) {
      const rangeArray = row[column].split("-");
      if (rangeArray.length == 2) {
        const rangeValues = range(+rangeArray[0], +rangeArray[1], 5);
        for (const d of rangeValues) {
          data.push({ ...row, [column]: d.toString() });
        }
      } else {
        data.push(row);
      }
    } else if (/-?\d*\.{0,1}\d+/.test(row[column])) {
      const dipMicroReg = row[column].toString().match(/-?\d*\.{0,1}\d+/g);
      if (dipMicroReg.length === 3) {
        const rangeValues = range(
          +dipMicroReg[0],
          +dipMicroReg[1],
          +dipMicroReg[2]
        );
        for (const d of rangeValues) {
          data.push({ ...row, [column]: d });
        }
      } else {
        data.push(row);
      }
    } else {
      data.push(row);
    }
  }
  return data;
};

app.get("/", function (req, res) {
  res.render("Home");
});

app.post("/columns", function (req, res, next) {
  // Error MiddleWare for multer file upload, so if any
  // error occurs, the image would not be uploaded!
  upload(req, res, function (err) {
    /* // FILE SIZE ERROR
    if (err instanceof multer.MulterError) {
      return res.render("Columns", { err: "Max file size 2MB allowed!" });
    }

    // INVALID FILE TYPE, message will return from fileFilter callback
    else  */ if (err) {
      return res.render("Columns", { err: err.message });
    }

    // FILE NOT SELECTED
    else if (!req.file) {
      return res.render("Columns", { err: "File is required!" });
    } else {
      const file = reader.readFile("./uploads/" + req.file.filename);
      const sheets = file.SheetNames;
      if (sheets.length > 0) {
        const rows = reader.utils.sheet_to_json(file.Sheets[sheets[0]]);
        if (rows.length > 0) {
          const columns = Object.keys(rows[1]);
          return res.render("Columns", {
            err: "",
            filename: req.file.filename,
            columns,
          });
        }
      }
      return res.render("Columns", { err: "File sheet or columns not found!" });
    }
  });
});

app.post("/result", function (req, res, next) {
  console.log(req.body);
  if (!req.body.filename) {
    return res.render("Result", { err: "Filename not found!" });
  }

  // Reading our test file
  const file = reader.readFile("./uploads/" + req.body.filename);

  const sheets = file.SheetNames;
  console.log("sheets total", sheets.length);
  const rows = reader.utils.sheet_to_json(file.Sheets[sheets[0]]);
  let data = [...rows];
  if (
    req.body.column instanceof String ||
    typeof req.body.column === "string"
  ) {
    data = microData(data, req.body.column);
  } else if (Array.isArray(req.body.column)) {
    for (const column of req.body.column) {
      data = [...microData(data, column)];
    }
  }

  // Printing data
  console.log("total", data.length);
  const filename = "results/" + req.body.filename;

  fs.existsSync("./" + filename, (exists) => {
    if (exists) {
      fs.unlinkSync("./" + filename);
    }
  });
  const result = reader.utils.book_new();
  const ws = reader.utils.json_to_sheet(data);
  reader.utils.book_append_sheet(result, ws, "Sheet1");
  reader.writeFile(result, "./" + filename);

  // SUCCESS, image successfully uploaded
  res.render("Result", { err: "", filename });
});

app.post("/api/columns", function (req, res, next) {
  // Error MiddleWare for multer file upload, so if any
  // error occurs, the image would not be uploaded!
  upload(req, res, function (err) {
    /* // FILE SIZE ERROR
    if (err instanceof multer.MulterError) {
      return res.render("Columns", { err: "Max file size 2MB allowed!" });
    }

    // INVALID FILE TYPE, message will return from fileFilter callback
    else  */ if (err) {
      return res.send({ err: err.message });
    }

    // FILE NOT SELECTED
    else if (!req.file) {
      return res.send({ err: "File is required!" });
    } else {
      const file = reader.readFile("./uploads/" + req.file.filename);
      const sheets = file.SheetNames;
      if (sheets.length > 0) {
        const rows = reader.utils.sheet_to_json(file.Sheets[sheets[0]]);
        if (rows.length > 0) {
          const columns = Object.keys(rows[1]);
          return res.send({ filename: req.file.filename, columns });
        }
      }
      return res.send({ err: "File sheet or columns not found!" });
    }
  });
});

app.post("/api/result", function (req, res, next) {
  console.log(req.body);
  if (!req.body.filename) {
    return res.send({ err: "Filename not found!" });
  }

  // Reading our test file
  const file = reader.readFile("./uploads/" + req.body.filename);

  const sheets = file.SheetNames;
  console.log("sheets total", sheets.length);
  const rows = reader.utils.sheet_to_json(file.Sheets[sheets[0]]);
  let data = [...rows];
  let columns = [];
  let newData = [];
  for (const c of req.body.newColumns) {
    if (c.splitWith) columns.push(c.dbName);
    for (let i = 0; i < data.length; i++) {
      newData[i] = {
        ...newData[i],
        [c.dbName]:
          typeof c.dbValue == "string"
            ? data[i][c.dbValue]
            : c.dbValue.map((col) => data[i][col]).join(" "),
      };
    }
  }
  for (const column of columns) {
    console.log("column", column);
    newData = [...microData(newData, column)];
  }

  // Printing data
  console.log("total", newData.length);
  const filename = "results/" + req.body.filename;

  fs.existsSync("./" + filename, (exists) => {
    if (exists) {
      fs.unlinkSync("./" + filename);
    }
  });
  const result = reader.utils.book_new();
  const ws = reader.utils.json_to_sheet(newData);
  reader.utils.book_append_sheet(result, ws, "Sheet1");
  reader.writeFile(result, "./" + filename);

  // SUCCESS, image successfully uploaded
  res.send({ filename });
});

app.listen(8080, function (error) {
  if (error) throw error;
  console.log("Server created Successfully on PORT 8080");
});
